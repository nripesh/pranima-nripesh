#ifndef BSTL_H
#define BSTL_H

#include"binarysearchtree.h"
class node {
public:
	int info;
	node* right;
	node* left;
};
class linkedBST :public BinarySearchTree {
public:
	node* root;
	linkedBST();
	void preorderTraversal(int index);
	void add(int data);
	bool Search(int data);
	bool isEmpty();
	int min();
	int max();
	void del(int data);
	node* del(node* root,int key);
	void inorder(node* n);
	void inorder();
	node* minvalnode(node* n);
	int max(node* n);
};

#endif
