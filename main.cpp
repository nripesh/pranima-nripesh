#include"bstl.h"
#include"arraybst.h"
#include <iostream>
using namespace std;

int main() {
	linkedBST tree;
	tree.add(10);
	tree.add(14);
	tree.add(5);
	tree.add(6);
	tree.add(1);
	tree.add(12);
	tree.add(15);
	tree.preorderTraversal(1);
	std::cout<<std::endl;
	std::cout<<"min value is:"<<tree.min();
	cout<<endl;
	tree.inorder();
	tree.del(10);
	tree.inorder();

	
	ArrayBST a;
	a.add(5);
	a.add(4);
	a.add(10);
	a.add(6);
	a.add(1);
	a.add(7);
	a.add(8);
	
	
	std::cout<<std::endl;
	
	a.preorderTraversal(1);
	
    if(a.Search(6)){
        std::cout<<"6 present in BST"<<std::endl;
    }
    else{
        std::cout<<"6 not present in BST"<<std::endl;
    }
    
    std::cout<<"Maximum term is: "<<a.max()<<std::endl;

}

