#include"bstl.h"
#include"stack.h"
using namespace std;

linkedBST::linkedBST() {
	root = NULL;
}
void linkedBST::preorderTraversal(int index) {
	stack st;
	st.push(root);
	node* a;
	while (!st.isEmpty()) {
		a=st.pop();
		cout << a->info << " ";
		if (a->right != NULL)	st.push(a->right);
		if (a->left != NULL)	st.push(a->left);
	}
}
void linkedBST::add(int data) {
	if (isEmpty()) {
		root = new node();
		root->info = data;
	}
	else {
		node* newnode = root;
		node* pre = NULL;
		bool l = false;
		bool r = false;
		while (newnode != NULL) {
			pre = newnode;
			l = false;
			if (data < newnode->info) {
				newnode = newnode->left;
				l = true;
			}
			else if (data > newnode->info) {
				newnode = newnode->right;
			}
		}
		if (l) {
			node* n = new node;
			n->info = data;
			pre->left = n;
		}
		else {
			node* n = new node;
			n->info = data;
			pre->right = n;
		}
	}
}
bool linkedBST::Search(int data){
	if(isEmpty()){
        cout<<"empty tree"<<endl;
		return false;
    }
    else{
        node *p=new node();
        p=root;
        while(p){
            if(data>p->info){
                p=p->right;
            }
            else if(data<p->info){
                p=p->left;
            }
            else if(data==p->info){
                cout<<data<<"data exists"<<endl;
                return true;

            }
            else{
                cout<<data<<"data doesn't exist"<<endl;
                return false;
            }
        }
    }
    cout<<data<<"data doesn't exist"<<endl;
    return true;
}
bool linkedBST::isEmpty() {
	return root == NULL;
}
int linkedBST::min(){
	node* p=root;
	while(p->left!=NULL){
		p=p->left;
	}
	return p->info;
}
int linkedBST::max(){
	return max(root);
}
int linkedBST::max(node* n){
	node* p=n;
	while(p->right!=NULL){
		p=p->right;
	}
	return p->info;
}
void linkedBST::inorder() {
	inorder(root);
}
void linkedBST::del(int data){
	del(root,data);
}
node* linkedBST::del(node* root,int key){
	if (root == NULL) return root;  
    if (key < root->info) 
        root->left = del(root->left, key); 
    else if (key > root->info) 
        root->right = del(root->right, key); 
    else
    { 
        if (root->left == NULL) 
        { 
            node* temp = root->right; 
            delete(root); 
            return temp; 
        } 
        else if (root->right == NULL) 
        { 
            node* temp = root->left; 
            delete(root); 
            return temp; 
        } 
        struct node* temp = minvalnode(root->right);
        root->info = temp->info; 

        root->right = del(root->right, temp->info); 
    } 
    return root; 
}

void linkedBST::inorder(node* n){
	stack s; 
    node* curr = root; 
    while (curr != NULL || s.isEmpty() == false) 
    { 
        while (curr !=  NULL) 
        { 
            s.push(curr); 
            curr = curr->left; 
        } 
        curr = s.Top(); 
        s.pop(); 
        cout << curr->info << " "; 
	
        curr = curr->right; 
    } 
}

node* linkedBST::minvalnode(node* n){
	node* current=n;
	while(current!=NULL && current->left!=NULL){
		current=current->left;
	}
	return current;
}
