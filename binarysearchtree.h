#ifndef BINARYSEARCHTREE_H
#define BINARYSEARCHTREE_H
#include<iostream>
class BinarySearchTree{
public:
    virtual void preorderTraversal(int index)=0;
    virtual void add(int data)=0;
    virtual bool Search(int data)=0;
    virtual int min()=0;
    virtual int max()=0;
    virtual void inorder()=0;
    virtual void del(int data)=0;
};

#endif // BINARYSEARCHTREE_H_INCLUDED
