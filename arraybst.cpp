#include <iostream>
#include "arraybst.h"

ArrayBST::ArrayBST(){
	for(int i=0;i<MAXSIZE;i++){
		this->element[i]=0;
	}
}

ArrayBST::~ArrayBST(){
	
}

void ArrayBST::add(int data){
	if(this->element[0]==0){
		element[1]=data;
	}
	else{
		for(int i=1;i<=MAXSIZE; ){
			if(data<this->element[i]){
				i=2*i;
			}
			else{
				i=2*i+1;
			}
			if(this->element[i]==0){
				this->element[i]=data;
				break;
			}
		}
	}
	this->element[0]=this->element[0]+1;
}

int ArrayBST::max(){
	int current_index=0;
	int max_term=element[current_index];
	while(current_index<MAXSIZE-1){
		if (max_term<element[current_index+1] ){
			max_term=element[current_index+1];
    }
    current_index++;
	}
	return max_term;
}

int ArrayBST::get_left_child(int index){
    if(element[index]!=0 && (2*index)<=MAXSIZE){
        return 2*index;
    }
    return -1;
}

int ArrayBST::get_right_child(int index){
    if(element[index]!=0 && (2*index+1)<=MAXSIZE){
        return 2*index+1;
    }
    return -1;
}

void ArrayBST::preorderTraversal(int index){
	if(index>0 && element[index]!=0)
    {
        std::cout<<element[index]<<std::endl;
        preorderTraversal(get_left_child(index));
        preorderTraversal(get_right_child(index));
    }
}

bool ArrayBST::Search(int data){
	int current_index=1;
	while(current_index<=MAXSIZE){
	    if(element[current_index]==data){
	        return true;
	        break;
	    }
	    else if(element[current_index]>data){
	        current_index=2*current_index;
	    }
	    else if(element[current_index]<data){
	        current_index=2*current_index+1;
	    }
	}
	return false;
}
int ArrayBST::min(){
	return 1;
}
int ArrayBST::inorder(){
	
}

int ArrayBST::del(int data){
	
}
